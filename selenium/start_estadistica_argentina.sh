#!/bin/sh
PROCESO=`ps -efa | grep 'estadistica_argentina.py' | grep -v vim | grep -v grep | grep -v 'start_estadistica_argentina.sh'`
if [ -z "$PROCESO"  ] 
then
    cd /Data/gobierno/selenium/   
    /Data/selenium/env/bin/python estadistica_argentina.py  >> estadistica_argentina.log
    scp ./argentina_statics.csv ./argentina_statics.pdf fermin2510@dtionce.com:/home/fermin2510/gobierno/selenium/
fi

