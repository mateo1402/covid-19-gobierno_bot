use gobierno;

DROP TABLE IF EXISTS T_TELEGRAM_ESTADISTICAS_TEMPORAL;
create table IF NOT EXISTS T_TELEGRAM_ESTADISTICAS_TEMPORAL (pais varchar(100) DEFAULT NOT NULL, total_casos varchar(100) DEFAULT NULL, nuevos_casos varchar(100) DEFAULT NULL, total_muertes varchar(100) DEFAULT NULL, nuevas_nuertes varchar(100) DEFAULT NULL, total_sanados varchar(100) DEFAULT NULL, casos_activos varchar(100) DEFAULT NULL, serios_criticos varchar(100) DEFAULT NULL, totcasos_1mpop varchar(100) DEFAULT NULL, totmuertes_1mpop varchar(100) DEFAULT NULL, primer_reportado varchar(100) DEFAULT NULL, PRIMARY KEY (`pais`)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

LOAD DATA LOCAL INFILE 'world_static.csv' into table T_TELEGRAM_ESTADISTICAS_TEMPORAL FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES; 

