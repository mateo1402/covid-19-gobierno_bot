#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging, logging.handlers
import telegram
import threading
import string
import random
import time
import ConfigParser, urllib2, MySQLdb
import sys
from MySQLdb import OperationalError
from datetime import datetime
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram import ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, Filters
from telegram.ext import RegexHandler,ConversationHandler, MessageHandler

# encoding=utf8
reload(sys)
sys.setdefaultencoding('utf8')

#logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)
#This is the right way of setting logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.handlers.TimedRotatingFileHandler("alerts.log", "midnight", 1)
#fh = logging.FileHandler('alerts.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

# Constantes
#TEXT_ALERT_PRODUCTS = 'Hay nuevos productos ingresados. /ver_productos_pendientes'
#TEXT_ALERT_RESERVAS = 'Hay nuevas reservas. /ver_reservas'

# Global
usuarios = {}
usuarios['admin'] = []
usuarios['users'] = []

#utils
def get_random_string(N=8):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))

# Funciones de DB
def fetch_data(query):
    global config
    try:
        db = MySQLdb.connect(host=config.get('Db','Host'), port=int(config.get('Db','Port')) , user=config.get('Db','Usuario'), passwd=config.get('Db','Contrasena'), db=config.get('Db','Db'), charset='utf8')
        db.autocommit(1)
        cursor=db.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)
        data=cursor.fetchall()
        cursor.close()
        db.close()
        return data
    except OperationalError as e:
        #reconnect()
        logger.error( 'MySQL Exception, trying again. %s ' % str(e) )
        fetch_data(query)

def permission(command, chat_id):
    query="select * from T_TELEGRAM_ALLOWED_WORDS where word='%(word)s' and mobile = %(chat_id)s ;" % {'word':command, 'chat_id' : chat_id}
    results=fetch_data(query)
    if results and len(results) > 0:
        return True
    return False

def marcar_reserva(idRes):
    query="UPDATE T_TELEGRAM_RESERVA_LOTES SET boSent=1 where idReserva=%d;" % (idRes)
    fetch_data(query)

def marcar_alertado(did):
    try:
        query="UPDATE T_TELEGRAM_PRODUCTOS SET boAlert=1 where id=%d;" % (did)
        fetch_data(query)
    except Exception as e:
        logger.error(str(e))

def marcar_enviado(product,sent):
    try:
        query="UPDATE T_TELEGRAM_PRODUCTOS SET boSent="+str(sent)+" where id=%d;" % (product['id'])
        fetch_data(query)
    except Exception as e:
        logger.error(str(e))

def reservas(availability=1):
    try:
        query='''SELECT *  FROM T_TELEGRAM_RESERVA_LOTES where
        boSent=0
        AND dtFecha BETWEEN DATE_SUB(NOW()+ INTERVAL 5 HOUR, INTERVAL 2 DAY) AND NOW()+ INTERVAL 5 HOUR
        ORDER BY idLote desc;'''
        #logger.debug(query)
        return fetch_data(query)
    except Exception as e:
        logger.error(str(e))

def productos(availability=1, alert=1, sent=0):
    try:
        query='''SELECT *  FROM T_TELEGRAM_PRODUCTOS where
        boAvailability=%(boAvailability)d
        and boAlert = %(boAlert)d
        and boSent = %(boSent)d
        ''' % {'boAvailability': availability, 'boAlert':alert, 'boSent': sent}
        #logger.debug(query)
        return fetch_data(query)
    except Exception as e:
        logger.error(str(e))

# Funciones
def actualizar_usuarios():
    try:
        query='''SELECT mobile from T_TELEGRAM_ALLOWED_WORDS where word='reservar';'''
        logger.debug(query)
        users = []
        for line in  fetch_data(query):
            users.append(line['mobile'])
        logger.debug(users)
        #admin
        query='''SELECT mobile from T_TELEGRAM_ALLOWED_WORDS where word='admin';'''
        logger.debug(query)
        admin = []
        for line in  fetch_data(query):
            admin.append(line['mobile'])
        logger.debug(admin)
        usuarios['admin'] = admin
        usuarios['users'] = users
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def alertar_reservas(bot):
    res = reservas(0)
    if not res or len(res)==0:
        return
    for r in res:
        for user in usuarios['admin']:
            try:
                send_reserva_description(bot, user, r)
            except Exception as e:
                logger.error('Error al contactarse con usuario %s' % user)
        marcar_reserva(r['idReserva'])

def send_reserva_description(bot, user, res):
    reserva = 'IdReserva: %d\nIdLote: %d\nFecha: %s\nUsuario: %s\nCantidad: %s\n' % (res['idReserva'], res['idLote'], res['dtFecha'], res['nombre'], res['nuCantidad'])
    try:
        bot.sendMessage(chat_id=user,text=reserva)
    except Exception as e:
        logger.error('Error al contactarse con usuario %s' % user)

def send_prod_alert_description(bot, user, product):
    global config
    keyboard = [[
                 InlineKeyboardButton("ACEPTAR", callback_data='/aceptar')
                 ],
                 [
                 InlineKeyboardButton("RECHAZAR", callback_data='/rechazar')
                 ]
                ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    #logger.debug('Sending image: %s' % dsImg)
    #try:
    #    fphoto = open(, 'rb')
    #except Exception as e:
    #    logger.warning('No se pudo encontrar la imagen del producto lote con id %d', lote.idlote)
    #    fphoto = open(SIN_FOTO, 'rb')
    #
    fphoto = str(config.get('Pics','Url'))
    fphoto += str(product['dsImg'])
    texto = "Descripción: " + str(product['dsDescription']) 

    query = "select * from T_TELEGRAM_LOG_NOMBRE where mobile="+str(product['mobile'])
    usuario = fetch_data(query)[0] 

    texto+= "\nNombre: " + str(usuario['first_name']) + "\nContacto: @"+str(usuario['username'])+""
    texto+= "\nPais: " + str(usuario['dsCountry']) + "\nProvincia: " + str(usuario['dsProvince'])
    texto+= "\nID: " + str(product['id'])
    if str(product['dsImg']) != 'sin_foto.jpg':
        texto+= "\n\n" + fphoto
 
    try:
        #msg = bot.sendPhoto(chat_id=user, photo=fphoto, caption="Hola", reply_markup=reply_markup)
        msg = bot.sendMessage(chat_id=user ,text=texto , reply_markup=reply_markup)
        idMsg  =  msg['message_id']
        mobile =  msg['chat']['id']
        idProduct = product['id']
        query = "INSERT INTO T_TELEGRAM_MENSAJES set idProduct='"+str(idProduct)+"' , mobile='"+str(mobile)+"' , idMsg='"+str(idMsg)+"' , fecha = now();" 
        fetch_data(query)
    except Exception as e:
        print ("Paso el exception")
        logger.error('Error enviando producto a user %s' % user)

def alertar_productos_nuevos(bot):
    products = productos(0,0,0)
    if not products or  len(products)==0:
        return
    for p in products:
        for user in usuarios['admin']:
            send_prod_alert_description(bot, user, p)
        marcar_alertado(p['id'])

# Comandos
def send_prod_description(bot, product):
    global config
    canal = config.get('Canal','Canal')
    logger.debug('Sending image: %s' % product['dsImg'])
    keyboard = []

    fphoto = str(config.get('Pics','Url'))
    fphoto += str(product['dsImg'])
    texto = "Descripción: " + str(product['dsDescription'])

    if str(product['dsImg']) != 'sin_foto.jpg':
        texto+= "\n\n" + fphoto

    try:
        msg = bot.sendMessage(chat_id= canal, text=texto)
        return msg.message_id
    except Exception as e:
        logger.warning('No se pudo encontrar enviar el producto con id %d' % (product['id']))
        return -1

def enviar_oferta_productos(bot):
    global config
    canal = config.get('Canal','Canal')
    products = productos(1, alert=1, sent=0)
    if not products or len(products)==0:
        return
    for p in products:
        try:
            msgid = send_prod_description(bot, p)
            if msgid!=-1:
	        marcar_enviado(p,1)
                query = "insert into T_TELEGRAM_MENSAJES (idProduct,mobile,idMsg,fecha,action) VALUES ("+str(p['id'])+",'"+str(canal)+"',"+str(msgid)+",now(),'PUSHED'); "
                fetch_data(query)
            else:
                marcar_enviado(p,-1)
        except Exception as e:
            logger.error('Error al enviar mensaje al canal')

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Read confir form file
config = ConfigParser.SafeConfigParser()
config.read('main.ini')

# Create the Updater and pass it your bot's token.
token = str(config.get('Telegram','token'))
bot = telegram.Bot(token=token)

while True:
    logger.debug('Durmiendo...')
    time.sleep(5)
    actualizar_usuarios()
    alertar_productos_nuevos(bot)
    #alertar_reservas(bot)
    enviar_oferta_productos(bot)
