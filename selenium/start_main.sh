#!/bin/sh
PROCESO=`ps -efa | grep 'Lomas_main.py' | grep -v vim | grep -v grep | grep -v 'start_main.sh'`
if [ -z "$PROCESO"  ] 
then
    cd /Data/gobierno/selenium/   
    /Data/selenium/env/bin/python lomas_main.py  >> lomas_main.log
    scp ./statics.csv ./world_statics.pdf fermin2510@dtionce.com:/home/fermin2510/gobierno/selenium/
fi

