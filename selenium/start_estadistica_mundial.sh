#!/bin/sh
PROCESO=`ps -efa | grep 'estadistica_mundial.py' | grep -v vim | grep -v grep | grep -v 'start_estadistica_mundial.sh'`
if [ -z "$PROCESO"  ] 
then
    cd /Data/gobierno/selenium/   
    /Data/selenium/env/bin/python estadistica_mundial.py  >> estadistica_mundial.log
    scp ./world_statics.csv ./world_statics.pdf fermin2510@dtionce.com:/home/fermin2510/gobierno/selenium/
fi

