import json, os, time , configparser, MySQLdb, pdfquery

import threading, random, shutil, subprocess, sys, string

import pandas as pd 

from random import randint

from time import sleep, gmtime,strftime

from MySQLdb import OperationalError

from selenium import webdriver

from selenium.webdriver.chrome.options import Options

from selenium.webdriver.support.ui import WebDriverWait

from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.common.by import By

from selenium.common.exceptions import TimeoutException

from selenium.common.exceptions import NoSuchElementException

from selenium.webdriver.remote.command import Command


config = configparser.ConfigParser()
archivo_config=sys.argv[0].replace('.py','.ini')
config.read(archivo_config)
alert_banned = ''
cantidad_populada = 0

# Funciones de DB
def fetch_data(query):
    global config
    try:
        db = MySQLdb.connect(charset='utf8', init_command='SET NAMES UTF8',host=config.get('Db','Host'), port=int(config.get('Db','Port')) , user=config.get('Db','Usuario'), passwd=config.get('Db','Contrasena'), db=config.get('Db','Db'))
        db.autocommit(1)
        cursor=db.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)
        data=cursor.fetchall()
        cursor.close()
        db.close()
        return data
    except OperationalError as e:
        #reconnect()
        print( 'MySQL Exception, trying again. %s ' % str(e) )
        #fetch_data(query)

def get_status(driver):
    try:
        driver.execute(Command.STATUS)
        return "Alive"
    except:
        return "Dead"

def wait_until_xpath_exists(driver, xpath, delay):
        try:
            xpath_is_present = EC.presence_of_element_located((By.XPATH, xpath))
            myElem = WebDriverWait(driver, delay).until(xpath_is_present)
            return True
        except TimeoutException:
            print("Looking for element ID timeout exceeded")
            return False

def check_exists_by_xpath(driver,xpath):
    try:
        driver.find_element(By.XPATH,xpath)
        #webdriver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True

def check_element(xpath,driver,delay):
    if wait_until_xpath_exists(driver, xpath, delay):
        if check_exists_by_xpath(driver, xpath):
                try:
                    return True
                except OperationalError as e:
                    if get_status(driver) == "Alive":
                        driver.quit()
                    return False
        else:
            if get_status(driver) == "Alive":
                driver.quit()
            return False
    else:
        #if get_status(driver) == "Alive":
        #    driver.quit()
        return False

def get_statics():
    fecha_comienzo = time.strftime('%X %x %Z')
    global config

    options = Options()
    options.add_argument("--disable-extensions")
    options.add_argument("--disable-gpu")
    options.add_argument("--headless")
    
    # Disabling images downloading for time improvements
    options.add_argument('--blink-settings=imagesEnabled=false')
    
    
    driver = webdriver.Chrome(config['Global']['chromedriver_path'], options=options)
    driver.set_window_size(1920, 1080)
    url='https://es.wikipedia.org/wiki/Pandemia_de_enfermedad_por_coronavirus_de_2020_en_Argentina'
    driver.get(url)

    sleep(1)

    titulos = ['Provincia', 'Casos\nConfirmados', 'Población', 'Total\nMuertes', 'Tot Casos\n/1M pop', 'Recuperados']
    #brands = driver.find_element_by_id('main_table_countries_today')
    #options = brands.find_elements_by_tag_name('thead')
    #for m in options:
    #    texto = []
    #    info = m.find_elements_by_tag_name('th')
    #    for t in info:
    #        texto.append(t.text.replace('\n',' '))
    #titulos=texto

    table_statics = []
    table_statics.append(titulos)
    #titulos = ['País\nOtro', 'Total\nCasos', 'Nuevos\nCasos', 'Total\nMuertes', 'Nuevas\nMuertes', 'Total\nSanados', 
    brands = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[4]/div/table[3]/tbody')
    options = brands.find_elements_by_tag_name('tr')
    for m in options:
        texto = []
        info = m.find_elements_by_tag_name('td')
        i=0
        for t in info:
            if i==0:
                texto.append(t.text.strip().replace(',','\n'))
            else:
                texto.append(t.text)
            i = i + 1 
        table_statics.append(texto)

    brands = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[4]/div/table[3]/tfoot')
    options = brands.find_elements_by_tag_name('tr')
    for m in options:
        texto = []
        info = m.find_elements_by_tag_name('th')
        i=0
        for t in info:
            if i==0:
                texto.append(t.text.strip().replace(' ',''))
            else:
                texto.append(t.text)
            i = i + 1
        table_statics.append(texto)


    pd.DataFrame(table_statics).to_csv("argentina_statics.csv",header=None, index=None)

    return table_statics


def convertir_pdf(data):

    fileName = 'argentina_statics.pdf'
    from reportlab.platypus import SimpleDocTemplate
    
    from reportlab.lib.pagesizes import A4
    
    
    
    pdf = SimpleDocTemplate(
    
        fileName,
    
        pagesize=A4
    
    )
    
    
    
    from reportlab.platypus import Table
    
    table = Table(data)
    
    
    
    # add style
    
    from reportlab.platypus import TableStyle
    
    from reportlab.lib import colors
    
    
    
    style = TableStyle([
    
        ('BACKGROUND', (0,0), (10,0), colors.cyan),
        ('BACKGROUND', (0,25), (-1,25), colors.dimgrey),
    
        ('TEXTCOLOR',(0,0),(-1,0),colors.black),

        ('TEXTCOLOR',(0,25),(-1,25),colors.whitesmoke),
        ('TEXTCOLOR',(0,1),(0,-2),colors.blue),
    
    
    
        ('ALIGN',(0,0),(-1,-1),'CENTER'),
    
    
    
        ('FONTNAME', (0,0), (-1,0), 'Courier-Bold'),
        ('FONTNAME', (0,2), (0,-1), 'Courier-Bold'),
    
        ('FONTSIZE', (0,0), (-1,0), 8.5),
        ('FONTSIZE', (0,1), (-1,-1), 8.5),
        ('FONTSIZE', (0,2), (0,-1), 8.5),
    
    
    
        ('BOTTOMPADDING', (0,0), (-1,0), 8.5),
    
    
    
    ])
    
    table.setStyle(style)
    
    
    
    # 2) Alternate backgroud color
    
    rowNumb = len(data)
    
    for i in range(1, rowNumb-1):

        if data[i][1] != '0' :
    
            bc_2 = colors.yellow
            tc_2 = colors.black
    
        else:
    
            bc_2 = colors.white
            tc_2 = colors.black


        if data[i][3] != '0' :

            bc_4 = colors.red
            tc_4 = colors.whitesmoke

        else:

            bc_4 = colors.white
            tc_4 = colors.black
 
              
        ts = TableStyle(
    
            [('BACKGROUND', (1,i),(1,i), bc_2),('TEXTCOLOR', (1,i), (1,i), tc_2),
             ('BACKGROUND', (3,i), (3,i), bc_4),('TEXTCOLOR', (3,i), (3,i),tc_4)]
    
        )
    
        table.setStyle(ts)
    
    
    # 3) Add borders
    
    ts = TableStyle(
    
        [
    
        ('BOX',(0,0),(-1,0),1,colors.black),
    
    
        ('GRID',(0,1),(-1,-1),1,colors.darkgrey),
    
        ]
    
    )
    
    table.setStyle(ts)
    
    
    
    elems = []
    
    elems.append(table)
    
    
    
    pdf.build(elems)
    
data = get_statics()
#data = [['País\nOtro', 'Total\nCasos', 'Nuevos\nCasos', 'Total\nMuertes', 'Nuevas\nMuertes', 'Total\nSanados', 'Activos\nCasos', 'Serios\nCríticos', 'Tot Casos\n/1M pop', 'Muertes\n/1M pop', 'Reportado\n1er. caso'], ['World', '857,299', '+72,561', '42,114', '+4,341', '177,141', '638,044', '32,297', '110.0', '5.4', 'Jan 10'], ['USA', '187,729', '+23,941', '3,867', '+726', '6,461', '177,401', '3,988', '567', '12', 'Jan 20'], ['Italy', '105,792', '', '12,428', '', '15,729', '77,635', '4,023', '1,750', '206', 'Jan 29']]
convertir_pdf(data)
