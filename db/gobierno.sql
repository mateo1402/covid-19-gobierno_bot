-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: mysql.bbsoft.us    Database: gobierno
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `T_TELEGRAM_ALLOWED_WORDS`
--

DROP TABLE IF EXISTS `T_TELEGRAM_ALLOWED_WORDS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_ALLOWED_WORDS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` bigint(20) NOT NULL,
  `word` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index_mobilew` (`mobile`,`word`)
) ENGINE=InnoDB AUTO_INCREMENT=2768 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_ALLOWED_WORDS`
--

LOCK TABLES `T_TELEGRAM_ALLOWED_WORDS` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_ALLOWED_WORDS` DISABLE KEYS */;
INSERT INTO `T_TELEGRAM_ALLOWED_WORDS` VALUES (2646,268202313,'admin'),(2649,268202313,'alias'),(2675,328771558,'alias'),(78,359062101,'admin'),(63,359062101,'alias'),(433,359062101,'cuantos'),(2655,371042409,'alias'),(2671,411346086,'alias'),(2709,1117850422,'alias');
/*!40000 ALTER TABLE `T_TELEGRAM_ALLOWED_WORDS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_TELEGRAM_CONSULTAS`
--

DROP TABLE IF EXISTS `T_TELEGRAM_CONSULTAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_CONSULTAS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telegram_mobile` bigint(20) NOT NULL,
  `fecha_consulta` datetime NOT NULL,
  `estado_consulta` varchar(20) NOT NULL,
  `texto_consulta` varchar(1000) NOT NULL,
  `texto_respuesta` varchar(1000) DEFAULT NULL,
  `fecha_respuesta` datetime DEFAULT NULL,
  `push_notif_only` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `consultas_mobile_idx` (`telegram_mobile`),
  KEY `estado_consulta_idx` (`estado_consulta`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_CONSULTAS`
--

LOCK TABLES `T_TELEGRAM_CONSULTAS` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_CONSULTAS` DISABLE KEYS */;
INSERT INTO `T_TELEGRAM_CONSULTAS` VALUES (1,359062101,'2020-03-30 20:25:15','CLOSED','Probando 2','ok','2020-03-30 20:25:30',0),(2,411346086,'2020-03-30 20:26:58','CLOSED','Que es river?','Riber es un equipo de la B','2020-03-30 20:27:36',0),(3,328771558,'2020-03-30 20:42:14','CLOSED','Coronavirus','ya se esta la vacuna','2020-03-30 20:44:02',0),(4,359062101,'2020-03-30 22:16:17','CLOSED','probando','ok','2020-03-30 18:50:38',0),(5,359062101,'2020-03-30 22:28:05','CLOSED','Probando 5','genail','2020-03-30 22:29:30',0),(6,411346086,'2020-03-31 06:27:25','CLOSED','Cuando termina la pandemia?','el 1 de julio','2020-03-31 06:30:30',0),(7,411346086,'2020-03-31 06:31:21','WAITINGFORRESPONSE','Sos una pc?',NULL,NULL,0),(8,359062101,'2020-03-31 07:57:26','CLOSED','Probando 6','ok','2020-03-31 08:16:29',0),(9,1117850422,'2020-03-31 07:59:13','CLOSED','Todo bien ?','no, porque estamos encerrados','2020-03-31 08:05:23',0),(10,1117850422,'2020-03-31 08:06:28','CLOSED','Y por un tiempo mas...','hasta el 1 de julio','2020-03-31 08:07:46',0),(11,1117850422,'2020-03-31 08:09:41','CLOSED','Estuve averiguando el tema de la fabrica de papel . No pastera, pero si la maquina q t comente. Con 2.5 millones masomenos se puede ingresar al mercado y ser parte de la mesa chica del papel en argentina','ok','2020-03-31 09:31:06',0),(12,359062101,'2020-03-31 08:16:44','CLOSED','CÃ³mo funciona esto!?','ok','2020-03-31 08:17:49',0),(13,359062101,'2020-03-31 08:26:13','CLOSED','Cómo funciona esto!?','Preguntas y una persona te responde.','2020-03-31 08:26:38',0),(14,359062101,'2020-03-31 08:35:31','CLOSED','Cómo cargar una fotő','ok','2020-03-31 08:35:50',0),(15,359062101,'2020-03-31 08:40:05','CLOSED','Próbándo acéntós őœ','ok','2020-03-31 08:40:25',0),(16,359062101,'2020-03-31 09:05:38','CLOSED','Próbándo distíntőøöœs acento§','ok','2020-03-31 09:06:12',0),(17,359062101,'2020-03-31 09:09:29','CLOSED','Pőœøöõôòóbando','ok','2020-03-31 09:09:55',0),(18,359062101,'2020-03-31 09:30:29','CLOSED','Próòôõöøőįőœīndo los acéêēęntos y caracteres raros','ok','2020-03-31 09:30:55',0);
/*!40000 ALTER TABLE `T_TELEGRAM_CONSULTAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_TELEGRAM_ENVIO_LOTES`
--

DROP TABLE IF EXISTS `T_TELEGRAM_ENVIO_LOTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_ENVIO_LOTES` (
  `idEnvio` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `idLote` int(5) unsigned NOT NULL,
  `chat_id` bigint(20) NOT NULL,
  `message_id` varchar(200) NOT NULL,
  PRIMARY KEY (`idEnvio`),
  KEY `idLote` (`idLote`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_ENVIO_LOTES`
--

LOCK TABLES `T_TELEGRAM_ENVIO_LOTES` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_ENVIO_LOTES` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_TELEGRAM_ENVIO_LOTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_TELEGRAM_LOG`
--

DROP TABLE IF EXISTS `T_TELEGRAM_LOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_LOG` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mobile` bigint(20) DEFAULT NULL,
  `texto` varchar(250) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `mobile_listener` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `log_idx_mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_LOG`
--

LOCK TABLES `T_TELEGRAM_LOG` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_LOG` DISABLE KEYS */;
INSERT INTO `T_TELEGRAM_LOG` VALUES (1,359062101,'Publicar Noticia','2020-03-30 20:24:02','MILOMASBOT'),(2,359062101,'Consultas','2020-03-30 20:25:07','MILOMASBOT'),(3,411346086,'Que es river?','2020-03-30 20:26:06','MILOMASBOT'),(4,411346086,'Actualizar Perfil','2020-03-30 20:26:16','MILOMASBOT'),(5,411346086,'Consultas','2020-03-30 20:26:43','MILOMASBOT'),(6,411346086,'Chupala','2020-03-30 20:28:07','MILOMASBOT'),(7,328771558,'/start','2020-03-30 20:37:50','MILOMASBOT'),(8,328771558,'Actualizar Perfil','2020-03-30 20:37:54','MILOMASBOT'),(9,328771558,'Luis Adrián Espinosa','2020-03-30 20:38:49','MILOMASBOT'),(10,328771558,'Luis Adrián Espinosa','2020-03-30 20:38:58','MILOMASBOT'),(11,328771558,'Actualizar Perfil','2020-03-30 20:39:05','MILOMASBOT'),(12,328771558,'Actualizar Perfil','2020-03-30 20:39:32','MILOMASBOT'),(13,328771558,'Consultas','2020-03-30 20:42:00','MILOMASBOT'),(14,328771558,'Consultas','2020-03-30 20:42:27','MILOMASBOT'),(15,328771558,'Publicar Noticia','2020-03-30 20:43:10','MILOMASBOT'),(16,328771558,'Consultas','2020-03-30 20:43:32','MILOMASBOT'),(17,328771558,'Publicar Noticia','2020-03-30 20:47:22','MILOMASBOT'),(18,359062101,'Consultas','2020-03-30 22:16:12','MILOMASBOT'),(19,359062101,'Consultas','2020-03-30 22:19:12','MILOMASBOT'),(20,359062101,'cuantos','2020-03-30 18:50:06','MILOMASBOT'),(21,359062101,'Consultas','2020-03-30 18:50:18','MILOMASBOT'),(22,359062101,'Publicar Noticia','2020-03-30 18:50:46','MILOMASBOT'),(23,359062101,'Publicar Noticia','2020-03-30 22:26:20','MILOMASBOT'),(24,359062101,'Consultas','2020-03-30 22:28:00','MILOMASBOT'),(25,359062101,'Consultas','2020-03-30 22:29:05','MILOMASBOT'),(26,268202313,'Hola','2020-03-31 04:02:09','MILOMASBOT'),(27,268202313,'Publicar Noticia','2020-03-31 04:02:20','MILOMASBOT'),(28,268202313,'Publicar Noticia','2020-03-31 04:08:43','MILOMASBOT'),(29,268202313,'Hcd  https://www.inforegion.com.ar/2020/03/30/lomas-se-esta-preparando-para-el-peor-de-los-casos-aseguro-chavez/','2020-03-31 04:09:12','MILOMASBOT'),(30,268202313,'Publicar Noticia','2020-03-31 04:09:19','MILOMASBOT'),(31,268202313,'https://www.inforegion.com.ar/2020/03/30/lomas-se-esta-preparando-para-el-peor-de-los-casos-aseguro-chavez/','2020-03-31 04:09:35','MILOMASBOT'),(32,268202313,'Publicar Noticia','2020-03-31 04:10:21','MILOMASBOT'),(33,268202313,'Probando','2020-03-31 04:13:59','MILOMASBOT'),(34,268202313,'Publicar Noticia','2020-03-31 04:14:06','MILOMASBOT'),(35,411346086,'Actualizar Perfil','2020-03-31 06:25:24','MILOMASBOT'),(36,411346086,'Pregunto?','2020-03-31 06:26:42','MILOMASBOT'),(37,411346086,'Consultas','2020-03-31 06:27:05','MILOMASBOT'),(38,411346086,'Consultas','2020-03-31 06:31:07','MILOMASBOT'),(39,268202313,'Consultas','2020-03-31 06:58:46','MILOMASBOT'),(40,268202313,'Consultas','2020-03-31 06:59:08','MILOMASBOT'),(41,1117850422,'/start','2020-03-31 07:39:24','MILOMASBOT'),(42,1117850422,'Actualizar Perfil','2020-03-31 07:39:29','MILOMASBOT'),(43,1117850422,'Hola','2020-03-31 07:41:58','MILOMASBOT'),(44,1117850422,'Hola','2020-03-31 07:42:08','MILOMASBOT'),(45,1117850422,'Publicar Noticia','2020-03-31 07:42:17','MILOMASBOT'),(46,1117850422,'Hola','2020-03-31 07:42:37','MILOMASBOT'),(47,1117850422,'Hola','2020-03-31 07:42:48','MILOMASBOT'),(48,1117850422,'Hola','2020-03-31 07:43:19','MILOMASBOT'),(49,1117850422,'Publicar Noticia','2020-03-31 07:43:25','MILOMASBOT'),(50,1117850422,'/','2020-03-31 07:43:56','MILOMASBOT'),(51,1117850422,'Buenos días milomas','2020-03-31 07:44:40','MILOMASBOT'),(52,1117850422,'Actualizar Perfil','2020-03-31 07:44:53','MILOMASBOT'),(53,1117850422,'Publicar Noticia','2020-03-31 07:45:06','MILOMASBOT'),(54,1117850422,'Publicar Noticia','2020-03-31 07:46:17','MILOMASBOT'),(55,1117850422,'Que hora es ?','2020-03-31 07:46:42','MILOMASBOT'),(56,1117850422,'Publicar Noticia','2020-03-31 07:46:48','MILOMASBOT'),(57,1117850422,'Cómo funciona esto ? Para que es ?','2020-03-31 07:47:33','MILOMASBOT'),(58,1117850422,'Publicar Noticia','2020-03-31 07:47:38','MILOMASBOT'),(59,359062101,'Consultas','2020-03-31 07:54:14','MILOMASBOT'),(60,359062101,'Probando','2020-03-31 07:57:17','MILOMASBOT'),(61,359062101,'Consultas','2020-03-31 07:57:21','MILOMASBOT'),(62,1117850422,'Todo bien ?','2020-03-31 07:58:30','MILOMASBOT'),(63,1117850422,'Todo bien','2020-03-31 07:58:42','MILOMASBOT'),(64,1117850422,'Publicar Noticia','2020-03-31 07:58:46','MILOMASBOT'),(65,1117850422,'Todo bien ?','2020-03-31 07:59:04','MILOMASBOT'),(66,1117850422,'Consultas','2020-03-31 07:59:08','MILOMASBOT'),(67,1117850422,'Recibis mis consultas ?','2020-03-31 08:00:22','MILOMASBOT'),(68,1117850422,'Consultas','2020-03-31 08:00:26','MILOMASBOT'),(69,1117850422,'Recibis mis consultas?','2020-03-31 08:00:35','MILOMASBOT'),(70,1117850422,'Y por un tiempo mas...','2020-03-31 08:06:05','MILOMASBOT'),(71,1117850422,'Consultas','2020-03-31 08:06:09','MILOMASBOT'),(72,1117850422,'Consultas','2020-03-31 08:07:01','MILOMASBOT'),(73,1117850422,'Estuve averiguando el tema de la fábrica de papel . No pastera, pero si la maquina q t comente','2020-03-31 08:07:50','MILOMASBOT'),(74,1117850422,'Y fábricas actuales y situacion de cada una  y costo','2020-03-31 08:08:24','MILOMASBOT'),(75,1117850422,'Consultas','2020-03-31 08:08:37','MILOMASBOT'),(76,1117850422,'Publicar Noticia','2020-03-31 08:15:13','MILOMASBOT'),(77,1117850422,'Publicar Noticia','2020-03-31 08:15:45','MILOMASBOT'),(78,359062101,'Consultas','2020-03-31 08:16:08','MILOMASBOT'),(79,359062101,'Consultas','2020-03-31 08:16:36','MILOMASBOT'),(80,359062101,'Consultas','2020-03-31 08:26:02','MILOMASBOT'),(81,1117850422,'Consultas','2020-03-31 08:27:17','MILOMASBOT'),(82,1117850422,'Nicolas','2020-03-31 08:27:21','MILOMASBOT'),(83,1117850422,'Publicar Noticia','2020-03-31 08:27:27','MILOMASBOT'),(84,359062101,'Publicar Noticia','2020-03-31 08:27:44','MILOMASBOT'),(85,359062101,'Consultas','2020-03-31 08:35:18','MILOMASBOT'),(86,359062101,'Publicar Noticia','2020-03-31 08:36:14','MILOMASBOT'),(87,359062101,'Consultas','2020-03-31 08:39:41','MILOMASBOT'),(88,359062101,'Publicar Noticia','2020-03-31 08:40:32','MILOMASBOT'),(89,1117850422,'Publicar Noticia','2020-03-31 08:42:10','MILOMASBOT'),(90,1117850422,'Consultas','2020-03-31 08:50:21','MILOMASBOT'),(91,1117850422,'Publicar Noticia','2020-03-31 08:50:26','MILOMASBOT'),(92,359062101,'Publicar Noticia','2020-03-31 08:58:30','MILOMASBOT'),(93,359062101,'Consultas','2020-03-31 09:00:40','MILOMASBOT'),(94,359062101,'Publicar Noticia','2020-03-31 09:08:02','MILOMASBOT'),(95,359062101,'Consultas','2020-03-31 09:09:08','MILOMASBOT'),(96,359062101,'Publicar Noticia','2020-03-31 09:10:25','MILOMASBOT'),(97,359062101,'Publicar Noticia','2020-03-31 09:27:03','MILOMASBOT'),(98,359062101,'Consultas','2020-03-31 09:30:24','MILOMASBOT'),(99,359062101,'Publicar Noticia','2020-03-31 09:38:06','MILOMASBOT');
/*!40000 ALTER TABLE `T_TELEGRAM_LOG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_TELEGRAM_LOG_NOMBRE`
--

DROP TABLE IF EXISTS `T_TELEGRAM_LOG_NOMBRE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_LOG_NOMBRE` (
  `mobile` bigint(20) NOT NULL DEFAULT '0',
  `first_name` varchar(250) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `dsName` varchar(250) DEFAULT NULL,
  `dsCountry` varchar(250) DEFAULT NULL,
  `dsProvince` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`mobile`),
  KEY `log_nombre_idx_mobile` (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_LOG_NOMBRE`
--

LOCK TABLES `T_TELEGRAM_LOG_NOMBRE` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_LOG_NOMBRE` DISABLE KEYS */;
INSERT INTO `T_TELEGRAM_LOG_NOMBRE` VALUES (268202313,'Javier','javieromontenegro','Javier','Argentina','Buenos Aires'),(328771558,'Luis Adrian','LuchoOk','Luis','Argentina','Salta'),(359062101,'Aríel','ariel2510','Ariel','Argentina','Buenos Aires'),(371042409,'Cristian','ccristian75','ccristian75','argentina','bsas'),(411346086,'Pablo','pablofperalta','PABLO','Argentina','Buenos Aires'),(1117850422,'Nico','NicoBronenberg','Consultas','Que hora es ?','Buenos Aires');
/*!40000 ALTER TABLE `T_TELEGRAM_LOG_NOMBRE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_TELEGRAM_LOTES`
--

DROP TABLE IF EXISTS `T_TELEGRAM_LOTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_LOTES` (
  `idLote` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `chat_id` bigint(20) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `nuPrecio` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `nuCantidad` int(6) unsigned NOT NULL DEFAULT '0',
  `totalStock` int(6) unsigned NOT NULL DEFAULT '0',
  `actualStock` int(6) unsigned NOT NULL DEFAULT '0',
  `dsDescription` longtext CHARACTER SET latin1,
  `dtFecha` datetime DEFAULT NULL,
  `dsImg` varchar(140) DEFAULT NULL,
  `boAvailability` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `boSent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `boAlert` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idLote`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_LOTES`
--

LOCK TABLES `T_TELEGRAM_LOTES` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_LOTES` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_TELEGRAM_LOTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_TELEGRAM_MENSAJES`
--

DROP TABLE IF EXISTS `T_TELEGRAM_MENSAJES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_MENSAJES` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idProduct` int(10) unsigned DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `idMsg` int(10) unsigned DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=latin2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_MENSAJES`
--

LOCK TABLES `T_TELEGRAM_MENSAJES` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_MENSAJES` DISABLE KEYS */;
INSERT INTO `T_TELEGRAM_MENSAJES` VALUES (1,1,'268202313',385,'2020-03-30 20:48:06','ACEPTADO'),(2,1,'359062101',386,'2020-03-30 20:48:08',NULL),(3,1,'@MILOMAS',27,'2020-03-30 20:48:59','PUSHED'),(4,2,'268202313',437,'2020-03-30 18:51:06',NULL),(5,2,'359062101',438,'2020-03-30 18:51:06','ACEPTADO'),(6,2,'@MILOMAS',30,'2020-03-30 18:51:11','PUSHED'),(7,3,'268202313',446,'2020-03-30 22:26:43',NULL),(8,3,'359062101',447,'2020-03-30 22:26:43','ACEPTADO'),(9,3,'@MILOMAS',31,'2020-03-30 22:27:09','PUSHED'),(10,4,'268202313',487,'2020-03-31 04:15:39','ACEPTADO'),(11,4,'359062101',488,'2020-03-31 04:15:39',NULL),(12,4,'@MILOMAS',32,'2020-03-31 04:15:49','PUSHED'),(13,5,'268202313',537,'2020-03-31 07:42:36','ACEPTADO'),(14,5,'359062101',538,'2020-03-31 07:42:36',NULL),(15,6,'268202313',551,'2020-03-31 07:43:52','ACEPTADO'),(16,6,'359062101',552,'2020-03-31 07:43:53',NULL),(17,6,'@MILOMAS',33,'2020-03-31 07:43:58','PUSHED'),(18,7,'268202313',570,'2020-03-31 07:45:19','ACEPTADO'),(19,7,'359062101',571,'2020-03-31 07:45:19',NULL),(20,8,'268202313',582,'2020-03-31 07:46:30','ACEPTADO'),(21,8,'359062101',583,'2020-03-31 07:46:31',NULL),(22,5,'@MILOMAS',34,'2020-03-31 07:51:40','PUSHED'),(23,7,'@MILOMAS',35,'2020-03-31 07:51:45','PUSHED'),(24,8,'@MILOMAS',36,'2020-03-31 07:51:50','PUSHED'),(25,9,'268202313',657,'2020-03-31 08:15:39','ACEPTADO'),(26,9,'359062101',658,'2020-03-31 08:15:39',NULL),(27,9,'@MILOMAS',37,'2020-03-31 08:16:25','PUSHED'),(28,11,'268202313',694,'2020-03-31 08:28:44','ACEPTADO'),(29,11,'359062101',695,'2020-03-31 08:28:44',NULL),(30,10,'268202313',708,'2020-03-31 08:38:07',NULL),(31,10,'359062101',709,'2020-03-31 08:38:08','ACEPTADO'),(32,12,'268202313',710,'2020-03-31 08:38:08',NULL),(33,12,'359062101',711,'2020-03-31 08:38:08','ACEPTADO'),(34,11,'@MILOMAS',38,'2020-03-31 08:38:08','PUSHED'),(35,10,'@MILOMAS',39,'2020-03-31 08:38:34','PUSHED'),(36,12,'@MILOMAS',40,'2020-03-31 08:38:34','PUSHED'),(37,13,'268202313',725,'2020-03-31 08:41:11',NULL),(38,13,'359062101',726,'2020-03-31 08:41:11','ACEPTADO'),(39,13,'@MILOMAS',41,'2020-03-31 08:41:16','PUSHED'),(40,14,'268202313',735,'2020-03-31 08:45:39','ACEPTADO'),(41,14,'359062101',736,'2020-03-31 08:45:39',NULL),(42,14,'@MILOMAS',42,'2020-03-31 08:46:20','PUSHED'),(43,15,'268202313',749,'2020-03-31 08:59:43',NULL),(44,15,'359062101',750,'2020-03-31 08:59:43','ACEPTADO'),(45,15,'@MILOMAS',43,'2020-03-31 08:59:59','PUSHED'),(46,16,'268202313',763,'2020-03-31 09:08:43',NULL),(47,16,'359062101',764,'2020-03-31 09:08:44','RECHAZADO'),(48,17,'268202313',777,'2020-03-31 09:10:40',NULL),(49,17,'359062101',778,'2020-03-31 09:10:40','RECHAZADO'),(50,18,'268202313',786,'2020-03-31 09:29:49',NULL),(51,18,'359062101',787,'2020-03-31 09:29:49','RECHAZADO'),(52,19,'268202313',801,'2020-03-31 09:38:54',NULL),(53,19,'359062101',802,'2020-03-31 09:38:54','ACEPTADO'),(54,19,'@MILOMAS',44,'2020-03-31 09:39:05','PUSHED');
/*!40000 ALTER TABLE `T_TELEGRAM_MENSAJES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_TELEGRAM_PRODUCTOS`
--

DROP TABLE IF EXISTS `T_TELEGRAM_PRODUCTOS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_PRODUCTOS` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` bigint(20) NOT NULL,
  `nuPrecio` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `nuCantidad` int(6) unsigned NOT NULL DEFAULT '0',
  `dsDescription` longtext,
  `dtFecha` datetime DEFAULT NULL,
  `dsImg` varchar(140) DEFAULT NULL,
  `boAvailability` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `boSent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `boAlert` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_PRODUCTOS`
--

LOCK TABLES `T_TELEGRAM_PRODUCTOS` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_PRODUCTOS` DISABLE KEYS */;
INSERT INTO `T_TELEGRAM_PRODUCTOS` VALUES (1,328771558,0.00,0,'Consultas','2020-03-30 20:48:53','UDCSD065.jpg',1,1,1),(2,359062101,0.00,0,'prueba4','2020-03-30 18:51:10','78CGG0ZI.jpg',1,1,1),(3,359062101,0.00,0,'Probando','2020-03-30 22:27:03','90LY0OOP.jpg',1,1,1),(4,268202313,0.00,0,'Probando','2020-03-31 04:15:47','6VWT4Y2P.jpg',1,1,1),(5,1117850422,0.00,0,'Hola','2020-03-31 07:51:37','sin_foto.jpg',1,1,1),(6,1117850422,0.00,0,'Hola','2020-03-31 07:43:55','sin_foto.jpg',1,1,1),(7,1117850422,0.00,0,'Hola milomasbot','2020-03-31 07:51:44','sin_foto.jpg',1,1,1),(8,1117850422,0.00,0,'Consultas','2020-03-31 07:51:47','sin_foto.jpg',1,1,1),(9,1117850422,0.00,0,'Coaching','2020-03-31 08:16:22','sin_foto.jpg',1,1,1),(10,359062101,0.00,0,'Cómo poder càrgâr una f?to','2020-03-31 08:38:29','CEO1NHB5.jpg',1,1,1),(11,1117850422,0.00,0,'Nicolas','2020-03-31 08:36:51','DNG4QW12.jpg',1,1,1),(12,359062101,0.00,0,'Prueba 6','2020-03-31 08:38:33','QZ917HR0.jpg',1,1,1),(13,359062101,0.00,0,'Público atmósfera climátic?','2020-03-31 08:41:14','KVN61V5E.jpg',1,1,1),(14,1117850422,0.00,0,'Bobinas de papel','2020-03-31 08:46:18','B0ALQB07.jpg',1,1,1),(15,359062101,0.00,0,'Próbando acéntós de t?œdo tipo','2020-03-31 08:59:56','ME3KBLHJ.jpg',1,1,1),(16,359062101,0.00,0,'Pró?øöœõôòbando distintos acentos','2020-03-31 09:09:03','Q1J4JFPQ.jpg',2,0,1),(17,359062101,0.00,0,'Pró?øöœõôòbando distintos acentos','2020-03-31 09:10:49','F16P37RC.jpg',2,0,1),(18,359062101,0.00,0,'Próòôõöø???œ?ndo los acéê??ntos y caracteres raros','2020-03-31 09:30:21','13MFZI2L.jpg',2,0,1),(19,359062101,0.00,0,'Próòøöīőœø¡öòóôõ','2020-03-31 09:39:01','TPSTEESH.jpg',1,1,1);
/*!40000 ALTER TABLE `T_TELEGRAM_PRODUCTOS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_TELEGRAM_PROFILE`
--

DROP TABLE IF EXISTS `T_TELEGRAM_PROFILE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_PROFILE` (
  `chat_id` bigint(20) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_PROFILE`
--

LOCK TABLES `T_TELEGRAM_PROFILE` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_PROFILE` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_TELEGRAM_PROFILE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `T_TELEGRAM_RESERVA_LOTES`
--

DROP TABLE IF EXISTS `T_TELEGRAM_RESERVA_LOTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_TELEGRAM_RESERVA_LOTES` (
  `idReserva` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `chat_id` bigint(20) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `nuCantidad` int(6) unsigned NOT NULL DEFAULT '0',
  `idLote` int(5) unsigned NOT NULL,
  `dtFecha` datetime DEFAULT NULL,
  `boSent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idReserva`),
  KEY `idLote` (`idLote`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `T_TELEGRAM_RESERVA_LOTES`
--

LOCK TABLES `T_TELEGRAM_RESERVA_LOTES` WRITE;
/*!40000 ALTER TABLE `T_TELEGRAM_RESERVA_LOTES` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_TELEGRAM_RESERVA_LOTES` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-31  9:40:27
