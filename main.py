#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging, logging.handlers
import telegram
import threading
import string
import random
import ConfigParser, urllib2, MySQLdb
import sys
from MySQLdb import OperationalError
from datetime import datetime
#from lotes import LoteIngreso, LoteReserva, id_from_readable, stock_from_readable
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram import ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, Filters
from telegram.ext import RegexHandler,ConversationHandler, MessageHandler
from time import sleep

# encoding=utf8
reload(sys)
sys.setdefaultencoding('utf8')

#Esta conf es para ver de forma general el log de la libreria.
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)
#This is the right way of setting logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
#fh = logging.FileHandler('main.log')
fh = logging.handlers.TimedRotatingFileHandler("main.log", "midnight", 1)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

#Menus
profile_keyboard = [[telegram.KeyboardButton('Perfil')],[telegram.KeyboardButton('Manual Bot')]]
custom_keyboard = [[telegram.KeyboardButton('Noticia'),
                   telegram.KeyboardButton('Perfil'),
                   telegram.KeyboardButton('Consultas')],
                   [telegram.KeyboardButton('Manual Bot'),
                   telegram.KeyboardButton('Estadisticas Total')],
                   [telegram.KeyboardButton('Estadisticas Argentina')], 
                   #telegram.KeyboardButton('Estadisticas Provincial')], 
                   [telegram.KeyboardButton('Estadisticas Mundial')],
                   [telegram.KeyboardButton('EstadisticasxPais')], 
                   [telegram.KeyboardButton('Sismos Mundial')]] 
nobody_keyboard = [[telegram.KeyboardButton('/start')],[telegram.KeyboardButton('Manual Bot')]]
admin_keyboard =  [[telegram.KeyboardButton('Noticia'),
                   telegram.KeyboardButton('Perfil'),
                   telegram.KeyboardButton('Consultas')],
                   [telegram.KeyboardButton('Manual Bot'),
                   telegram.KeyboardButton('Estadisticas Total')],
                   [telegram.KeyboardButton('Estadisticas Argentina')], 
                   #telegram.KeyboardButton('Estadisticas Provincial')], 
                   [telegram.KeyboardButton('Estadisticas Mundial')], 
                   [telegram.KeyboardButton('EstadisticasxPais')],
                   [telegram.KeyboardButton('Sismos Mundial')]]

#Constantes
SIN_FOTO = 'sin_foto.jpg'
NO_PRODUCTS = 'No hay productos nuevos ingresados'
NOT_LOGGED_MSG = 'Hola! Para realizar operaciones debes indicar tu id al Administrador. Utiliza el comando /miid para saber tu id.'
BANNED_MSG = 'USER HAS BEEN BANNED'
SALUDO_EXPLICACION_SIN_PERFIL = '''Hola! Bienvenido a MILOMASBOT. Aquí recibirá noticias acerca del ministerio de Salud.Por Favor actualice su perfil.'''
SALUDO_EXPLICACION_CON_PERFIL = '''Hola! Bienvenido a MILOMASBOT. Aquí recibirá noticias del ministerio de Salud.'''
NOMBRE_MSG = 'Por favor, ingrese el nombre identificador del producto.'
INGRESO_MSG = 'Excelente, ha comenzado la carga de una noticia. Puede cancelar en cualquier momento haciendo click en /cancelar_noticia. A continuación, ingrese la descripción de la noticia y al final puede incluír un link de youtube o video a publicar.'
PHOTO_MSG = 'Por favor, ingrese una imagen o continue sin ingresar imagen /sin_foto'
CANTIDAD_POR_LOTE_MSG = 'Ingrese la cantidad de unidades por lote, con numeros'
CANTIDAD_MSG = 'Ingrese la cantidad de unidades disponibles.'
PRECIO_MSG = 'Ingrese precio por unidad en USDT'
PRECIO_ERROR = 'El formato del precio NO es el esperado. No incluya moneda. Ingrese el precio en USDT, por ejemplo: 39,99'
CANTIDAD_ERROR = 'El formato de la cantidad NO es el esperado. Por favor ingrese solo números.'
GRACIAS_INGRESO = 'Muchas Gracias. Su ingreso será procesado a la brevedad.'
ACTUALIZAR_PERFIL = 'Para continuar, por favor, actualice su perfil'
GRACIAS_INGRESO_PERFIL = 'Muchas Gracias por actualizar su perfil.'
ERROR_INGRESO = 'El ingreso del producto no pudo ser procesado. Por favor intente nuevamente.'
ERROR_INGRESO_PERFIL = 'El ingreso del perfil no pudo ser procesado. Por favor intente nuevamente.'
INGRESO_PERFIL_MSG = 'Excelente, ha comenzado la carga de su perfil. Puede cancelar en cualquier momento haciendo click en /cancelar_perfil. A continuación, ingrese su nombre.'
PAIS_MSG = 'Ingrese su país de residencia' 
PROVINCIA_MSG = 'Ingrese su provicia o estado de residencia'
DESCRIPCION, PHOTO, PRECIO, CANTIDAD = range(4)
NOMBRE, PAIS, PROVINCIA = range(3)
CONSULTA = 1
CONSULTA_PAIS = 1
AGREGAR_USUARIO_OK = 'El usuario ha sido agregado exitosamente.'
AGREGAR_USUARIO_ERROR = 'Error al intentar agregar al usuario.'
SALUDO_SIN_ALIAS = 'Para operar debes ingresar un alias de telegram y luego aprieta el boton /start'
UTILICE_TECLADO = 'Por favor, utilice la botonera para publicar noticias.'
INGRESO_CONSULTA = 'Ha comenzado el ingreso de su consulta (Por favor, hagalo en una sola frase) y presione Enviar. Puede cancelar en cualquier momento el ingreso de su consulta haciendo click en /cancelar_consulta'
INGRESO_CONSULTA_PAIS = 'Por favor ingrese el país que desea consultar las estadísticas y presione Enviar. Puede cancelar en cualquier momento el ingreso de su consulta haciendo click en /cancelar_estadistica_pais'
GRACIAS_INGRESO_CONSULTA = 'Hemos recibido tu consulta. Te la responderemos a la brevedad por este mismo canal.'
ERROR_INGRESO_CONSULTA = 'Su consulta no pudo ser ingresada.'
TIENE_CONSULTA_PENDIENTE = 'Tenemos una consulta previa en estado Pendiente de Responder. Podrás enviar una nueva consulta cuando hayamos respondido la consulta pendiente.'

# Global
producto = {}
perfil = {} 
consulta = {}
consulta_pais = {}
#utils
def get_random_string(N=8):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))

# Funciones de DB
def fetch_data(query):
    global config
    try:
        db = MySQLdb.connect(host=config.get('Db','Host'), port=int(config.get('Db','Port')) , user=config.get('Db','Usuario'), passwd=config.get('Db','Contrasena'), db=config.get('Db','Db'), charset='utf8')
        db.autocommit(1)
        cursor=db.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)
        data=cursor.fetchall()
        cursor.close()
        db.close()
        return data
    except OperationalError as e:
        #reconnect()
        logger.error( 'MySQL Exception, trying again. %s ' % str(e) )
        fetch_data(query)

def response_consultas():
    while True:
        query="select CAST(CONVERT(c.texto_consulta USING utf8) AS BINARY) as texto_consulta, CAST(CONVERT(c.texto_respuesta USING utf8) AS BINARY) as texto_respuesta, push_notif_only, CAST(CONVERT(n.first_name USING utf8) AS BINARY) as first_name, c.telegram_mobile, c.id  from T_TELEGRAM_CONSULTAS c join T_TELEGRAM_LOG_NOMBRE n on (c.telegram_mobile=n.mobile) where estado_consulta='UPDATEINGUSER';"
        respuestas = fetch_data(query)
        for respuesta in respuestas:
            print respuesta
            if respuesta['push_notif_only']==0:
                texto="Hola <b>"+str(respuesta['first_name'].title())+"\n\nConsulta</b>: "+str(respuesta['texto_consulta'])+"\n\n<b>Respuesta:</b> "+str(respuesta['texto_respuesta'])
            else:
                texto="Hola <b>"+str(respuesta['first_name'].title())+"</b>. "+str(respuesta['texto_respuesta'])
            print texto
            reply_markup = keyboard_correspondiente(respuesta['telegram_mobile'])
            try:
                bot.sendMessage(chat_id=respuesta['telegram_mobile'] ,text=texto , reply_markup=reply_markup, parse_mode="HTML")
            except:
                pass
            query="update T_TELEGRAM_CONSULTAS set estado_consulta='CLOSED' where id="+str(respuesta['id'])
            fetch_data(query)

        sleep(random.randint(1, 3))

def alta_usuario(miid):
    try:
        query='''INSERT into T_TELEGRAM_ALLOWED_WORDS (mobile, word) values (%s, 'ingresar_producto'), (%s, 'start');''' % (miid, miid, miid)
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def permission(command, chat_id):
    query="select * from T_TELEGRAM_ALLOWED_WORDS where word='%(word)s' and mobile = %(chat_id)s ;" % {'word':command, 'chat_id' : chat_id}
    results=fetch_data(query)
    if len(results) > 0:
        return True
    return False

def productos(availability):
    try:
        query='''SELECT *  FROM T_TELEGRAM_PRODUCTOS where
        boAvailability=%(boAvailability)d
        ;''' % {'boAvailability': availability}
        #logger.debug(query)
        return fetch_data(query)
    except Exception as e:
        logger.error(str(e))

def telegram_log(telegram_mensaje, telegram_from, username, first_name):
    query="insert into T_TELEGRAM_LOG set mobile="+str(telegram_from)+", texto='"+telegram_mensaje+"', mobile_listener='"+str(config.get('Telegram','robot'))+"', datetime=now(); insert into T_TELEGRAM_LOG_NOMBRE set mobile="+str(telegram_from)+", first_name='"+str(first_name)+"', username='"+str(username)+"'  ON DUPLICATE KEY UPDATE first_name='"+str(first_name)+"' , username='"+str(username)+"' ;"
    fetch_data(query)
    if username is None:
        query="delete from T_TELEGRAM_ALLOWED_WORDS where mobile="+str(telegram_from)+" and word='alias';"
    else:    
        query="insert ignore into T_TELEGRAM_ALLOWED_WORDS set mobile="+str(telegram_from)+",word='alias';"
    fetch_data(query)

def telegram_ingreso_producto(producto,mobile):
    try:
        descripcion = producto['descripcion']
        foto = producto['foto']
        #precio = producto['precio']
        precio = 0
        #cantidad = producto['cantidad'] 
        cantidad = 0 
        query="INSERT into T_TELEGRAM_PRODUCTOS set mobile='"+str(mobile)+"', nuPrecio='"+str(precio)+"', nuCantidad='"+str(cantidad)+"', dsDescription='"+str(descripcion)+"', dtFecha=now() , dsImg='"+str(foto)+"';"
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def telegram_ingreso_consulta(consulta,mobile):
    try:
        pregunta = consulta['consulta']
        query="INSERT into T_TELEGRAM_CONSULTAS set telegram_mobile='"+str(mobile)+"', fecha_consulta=now(), estado_consulta='OPEN', texto_consulta='"+str(pregunta)+"';"
        print "QUERY..............", query
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def telegram_ingreso_perfil(perfil,mobile):
    try:
        nombre = perfil['nombre']
        pais = perfil['pais']
        provincia = perfil['provincia']
        query="INSERT into T_TELEGRAM_LOG_NOMBRE set dsName='"+str(nombre)+"', dsCountry='"+str(pais)+"', dsProvince='"+str(provincia)+"', mobile='"+str(mobile)+"' ON DUPLICATE KEY UPDATE dsName='"+str(nombre)+"', dsCountry='"+str(pais)+"', dsProvince='"+str(provincia)+"';"
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def aceptar(did):
    try:
        query='''UPDATE T_TELEGRAM_PRODUCTOS
        set boAvailability=1, dtFecha = NOW() where id=%d;''' % int(did)
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def rechazar(did):
    try:
        query='''UPDATE T_TELEGRAM_PRODUCTOS
        set boAvailability=2, dtFecha = NOW() where id=%d;''' % int(did)
        logger.debug(query)
        fetch_data(query)
        return True
    except Exception as e:
        logger.error(str(e))
        return False

def product(bot, update):
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    if not valid_perfil(update.message.chat_id):
        # Respuesta para cuando el user tiene alias pero no tiene completo el perfil
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(ACTUALIZAR_PERFIL,reply_markup=reply_markup)
        return
    update.message.reply_text(INGRESO_MSG,reply_markup=keyboard_correspondiente(update.message.chat_id) )
    producto[update.message.chat_id] = {} 
    return DESCRIPCION

def profile(bot, update):
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    update.message.reply_text(INGRESO_PERFIL_MSG,reply_markup=keyboard_correspondiente(update.message.chat_id) )
    perfil[update.message.chat_id] = {} 
    return NOMBRE

def question(bot, update):
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    query="select count(*) as consultas from T_TELEGRAM_CONSULTAS where estado_consulta in ('OPEN','WAITINGFORRESPONSE') and telegram_mobile="+str(update.message.chat_id)
    consultas_pending=fetch_data(query)
    if consultas_pending[0]['consultas']!=0:
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(TIENE_CONSULTA_PENDIENTE,reply_markup=reply_markup)
        return
    update.message.reply_text(INGRESO_CONSULTA,reply_markup=keyboard_correspondiente(update.message.chat_id) )
    consulta[update.message.chat_id] = {} 
    return CONSULTA


def question_estadistica_pais(bot, update):
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    update.message.reply_text(INGRESO_CONSULTA_PAIS,reply_markup=keyboard_correspondiente(update.message.chat_id) )
    consulta_pais[update.message.chat_id] = {}
    return CONSULTA_PAIS


def timeout(bot, update):
    logger.error("User %s timeout the conversation." % user.first_name)
    update.message.reply_text('Se ha superado el tiempo limite para el ingreso del producto, por favor, comience la operación nuevamente',
                              reply_markup=keyboard_correspondiente(update.message.chat_id))
    if update.message.chat_id in ingresos:
        del ingresos[update.message.chat_id]
    return ConversationHandler.END

def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the product input." % user.username)
    update.message.reply_text('Se ha cancelado el ingreso del producto, por favor, comience la operación nuevamente',
                              reply_markup=keyboard_correspondiente(update.message.chat_id))
    if update.message.chat_id in producto:
        del producto[update.message.chat_id]
    return ConversationHandler.END

def cancel_profile(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the profile input." % user.username)
    update.message.reply_text('Se ha cancelado el ingreso del perfil, por favor, comience la operación nuevamente',
                              reply_markup=keyboard_correspondiente(update.message.chat_id))
    if update.message.chat_id in ingresos:
        del ingresos[update.message.chat_id]
    return ConversationHandler.END

def cancel_question(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the question input." % user.username)
    update.message.reply_text('Se ha cancelado el ingreso de la consulta, por favor, comience la operación nuevamente',
                              reply_markup=keyboard_correspondiente(update.message.chat_id))
    if update.message.chat_id in consulta:
        del consulta[update.message.chat_id]
    return ConversationHandler.END

def cancel_estadistica_pais(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the question of country input." % user.username)
    update.message.reply_text('Se ha cancelado el ingreso de la consulta de país, por favor, comience la operación nuevamente',
                              reply_markup=keyboard_correspondiente(update.message.chat_id))
    if update.message.chat_id in consulta_pais:
        del consulta_pais[update.message.chat_id]
    return ConversationHandler.END

def input_question(bot, update):
    user = update.message.from_user
    if update.message.chat_id in consulta:
        consulta[update.message.chat_id]['consulta'] = update.message.text
    else:
        cancel_question(bot, update)
    logger.info("El la consulta enviada por %s: %s" % (user.username, update.message.text))
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    if telegram_ingreso_consulta(consulta[update.message.chat_id],update.message.chat_id):
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(GRACIAS_INGRESO_CONSULTA, reply_markup=reply_markup)
    else:
        update.message.reply_text(ERROR_INGRESO_CONSULTA, reply_markup=reply_markup)

    if update.message.chat_id in consulta:
        del consulta[update.message.chat_id]
    return ConversationHandler.END

def nombre_perfil(bot, update):
    user = update.message.from_user
    if update.message.chat_id in perfil:
        perfil[update.message.chat_id]['nombre'] = update.message.text
    else:
        cancel_profile(bot, update)
    logger.info("El nombre de perfil enviado por %s: %s" % (user.username, update.message.text))
    update.message.reply_text(PAIS_MSG)
    return PAIS

def pais_perfil(bot, update):
    user = update.message.from_user
    if update.message.chat_id in perfil:
        perfil[update.message.chat_id]['pais'] = update.message.text
    else:
        cancel_profile(bot, update)
    logger.info("El pais de perfil enviado por %s: %s" % (user.username, update.message.text))
    update.message.reply_text(PROVINCIA_MSG)
    return PROVINCIA

def provincia_perfil(bot, update):
    user = update.message.from_user
    if update.message.chat_id in perfil:
        perfil[update.message.chat_id]['provincia'] = update.message.text
    else:
        cancel(bot, update)
    logger.info("la provincia de perfil enviado por %s: %s" % (user.username, update.message.text))
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    if telegram_ingreso_perfil(perfil[update.message.chat_id],update.message.chat_id):
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(GRACIAS_INGRESO_PERFIL, reply_markup=reply_markup)
    else:
        update.message.reply_text(ERROR_INGRESO_PERFIL, reply_markup=reply_markup)

    if update.message.chat_id in perfil:
        del perfil[update.message.chat_id]
    return ConversationHandler.END

def descripcion_producto(bot, update):
    user = update.message.from_user
    if update.message.chat_id in producto:
        producto[update.message.chat_id]['descripcion'] = update.message.text
    else:
        cancel(bot, update)
    logger.info("La descripcion enviada por %s: %s" % (user.username, update.message.text))
    update.message.reply_text(PHOTO_MSG)
    return PHOTO

def skip_foto_producto(bot, update):
    global config
    user = update.message.from_user
    if update.message.chat_id in producto:
        producto[update.message.chat_id]['foto'] = SIN_FOTO
    else:
        cancel(bot, update)
    logger.info("User %s NO envio foto" % user.username)
    #update.message.reply_text(PRECIO_MSG)
    #return PRECIO
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    if telegram_ingreso_producto(producto[update.message.chat_id],update.message.chat_id):
        update.message.reply_text(GRACIAS_INGRESO, reply_markup=reply_markup)
    else:
        update.message.reply_text(ERROR_INGRESO, reply_markup=reply_markup)

    if update.message.chat_id in producto:
        del producto[update.message.chat_id]
    return ConversationHandler.END


def foto_producto(bot, update):
    global config
    user = update.message.from_user
    photo_file = bot.getFile(update.message.photo[-1].file_id)
    photoname = get_random_string() + '.jpg'
    photofile = config.get('Pics','Path') + photoname
    photo_file.download(photofile)
    if update.message.chat_id in producto:
        producto[update.message.chat_id]['foto'] = photoname
    else:
        cancel(bot, update)
 
    logger.info("La photo enviada por %s: %s" % (user.username, photoname))
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    if telegram_ingreso_producto(producto[update.message.chat_id],update.message.chat_id):
        update.message.reply_text(GRACIAS_INGRESO, reply_markup=reply_markup)
    else:
        update.message.reply_text(ERROR_INGRESO, reply_markup=reply_markup)

    if update.message.chat_id in producto:
        del producto[update.message.chat_id]
    return ConversationHandler.END


def valid_perfil(mobile):
    query = "select IF(dsName is NULL or dsCountry is NULL or dsProvince is NULL,0,1) as valid_profile from T_TELEGRAM_LOG_NOMBRE where mobile='"+str(mobile)+"';"
    results = fetch_data(query)
    if len(results) > 0:
        if results[0]['valid_profile'] == 0 :
            return False
        else:
            return True


def keyboard_correspondiente(chat_id):
    keyboard = []
    #
    if permission('admin', chat_id):
        # Admin
        keyboard = admin_keyboard
    elif not permission('alias', chat_id):
        # Respuesta para start cuando no se conoce el usuario
        keyboard = nobody_keyboard
    else:
        # Respuesta si no tiene perfil actualizado o si tiene ya puede operar
        if not valid_perfil(chat_id): 
            keyboard = profile_keyboard
        else:
            keyboard = custom_keyboard

    return ReplyKeyboardMarkup(keyboard, resize_keyboard=True,
                                       one_time_keyboard=False)

def cuantos(bot, update):
    mobile=update.message.chat_id
    telegram_log(update.message.text, update.message.chat_id, update.message.chat['username'], update.message.chat['first_name'])
    if permission('cuantos', mobile) and permission('admin', mobile) and not permission('banned', mobile):
        query="select count(distinct mobile) as total_usuarios from T_TELEGRAM_ALLOWED_WORDS;"
        cuantos=fetch_data(query)[0]
        texto="<b>"+str(cuantos['total_usuarios'])+"</b> usuarios"

        query="select count(*) as total from T_TELEGRAM_CONSULTAS where estado_consulta in ('WAITINGFORRESPONSE','OPEN');"
        consultas=fetch_data(query)[0]
        if len(consultas)>0:
            if consultas['total']>0:
                texto+="\n<b>"+str(consultas['total'])+"</b> consultas pendientes"

        query="select count(*) as total, case boAvailability when 0 then 'Pendiente Aprobacion Admins' when 1 then 'Publicados' when 2 then 'Rechazados' END  as estado from T_TELEGRAM_PRODUCTOS group by boAvailability;"
        productos=fetch_data(query)
        if len(productos)>0:
            for p in productos:
                texto+="\n<b>"+str(p['total'])+"</b> Noticias "+str(p['estado'])
        #BoAvailability: Aceptado:1, Rechazado: 2, Pendiente de aprobacion admin: 0
        #boSent: pusheado al canal [0|1]
        #boAlert: producto alertado, o no, a admins para aprobacion [0|1]

        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(texto,reply_markup=reply_markup, parse_mode='HTML')
        
def miid(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    update.message.reply_text(update.message.chat_id)

def estadisticas_argentina(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    query = "SELECT * FROM T_TELEGRAM_ESTADISTICAS WHERE pais='Argentina';"
    data=fetch_data(query)
    if len(data)>0:
        texto = "País:  <b>" + str(data[0]['pais']) + " </b>\n"
        if str(data[0]['total_casos']) != '':
            texto += "Total_Casos:  <b> " + str(data[0]['total_casos']) + " </b>\n" 
        if str(data[0]['nuevos_casos']) != '':
            texto += "Nuevos_Casos:  <b> " + str(data[0]['nuevos_casos']) + " </b>\n"
        if str(data[0]['total_muertes']) != '':
            texto += "Total_Muertes:  <b> " + str(data[0]['total_muertes']) + " </b>\n"
        if str(data[0]['nuevas_muertes']) != '':
            texto += "Nuevas_Muertes:  <b> " + str(data[0]['nuevas_muertes']) + " </b>\n"
        if str(data[0]['total_sanados']) != '':
            texto += "Total_Sanados:  <b> " + str(data[0]['total_sanados']) + " </b>\n"
        if str(data[0]['casos_activos']) != '':
            texto += "Casos_Activos:  <b> " + str(data[0]['casos_activos']) + " </b>\n"
        if str(data[0]['serios_criticos']) != '':
            texto += "Serios_Criticos:  <b> " + str(data[0]['serios_criticos']) + " </b>\n"
        if str(data[0]['totcasos_1mpop']) != '':
            texto += "Tot. Casos/1Mpop:  <b> " + str(data[0]['totcasos_1mpop']) + " </b>\n"
        if str(data[0]['totmuertes_1mpop']) != '':
            texto += "Tot. Muertes/1Mpop:  <b> " + str(data[0]['totmuertes_1mpop']) + " </b>\n"
        if str(data[0]['total_tests']) != '':
            texto += "Total Tests:  <b> " + str(data[0]['total_tests']) + " </b>\n"
        if str(data[0]['tests_1mpop']) != '':
            texto += "Tot. Tests/1Mpop:  <b> " + str(data[0]['tests_1mpop']) + " </b>\n"

    bot.sendMessage(chat_id=update.message.chat_id ,text=texto , reply_markup=reply_markup, parse_mode='HTML') 

def estadisticas_mundial(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    query = "SELECT * FROM T_TELEGRAM_ESTADISTICAS WHERE pais='World';"
    data=fetch_data(query)
    if len(data)>0:
        texto = "País:  <b>" + str(data[0]['pais']) + " </b>\n"
        if str(data[0]['total_casos']) != '':
            texto += "Total_Casos:  <b> " + str(data[0]['total_casos']) + " </b>\n"
        if str(data[0]['nuevos_casos']) != '':
            texto += "Nuevos_Casos:  <b> " + str(data[0]['nuevos_casos']) + " </b>\n"
        if str(data[0]['total_muertes']) != '':
            texto += "Total_Muertes:  <b> " + str(data[0]['total_muertes']) + " </b>\n"
        if str(data[0]['nuevas_muertes']) != '':
            texto += "Nuevas_Muertes:  <b> " + str(data[0]['nuevas_muertes']) + " </b>\n"
        if str(data[0]['total_sanados']) != '':
            texto += "Total_Sanados:  <b> " + str(data[0]['total_sanados']) + " </b>\n"
        if str(data[0]['casos_activos']) != '':
            texto += "Casos_Activos:  <b> " + str(data[0]['casos_activos']) + " </b>\n"
        if str(data[0]['serios_criticos']) != '':
            texto += "Serios_Criticos:  <b> " + str(data[0]['serios_criticos']) + " </b>\n"
        if str(data[0]['totcasos_1mpop']) != '':
            texto += "Tot. Casos/1Mpop:  <b> " + str(data[0]['totcasos_1mpop']) + " </b>\n"
        if str(data[0]['totmuertes_1mpop']) != '':
            texto += "Tot. Muertes/1Mpop:  <b> " + str(data[0]['totmuertes_1mpop']) + " </b>\n"
        if str(data[0]['total_tests']) != '':
            texto += "Total Tests:  <b> " + str(data[0]['total_tests']) + " </b>\n"
        if str(data[0]['tests_1mpop']) != '':
            texto += "Tot. Tests/1Mpop:  <b> " + str(data[0]['tests_1mpop']) + " </b>\n"

    bot.sendMessage(chat_id=update.message.chat_id ,text=texto , reply_markup=reply_markup, parse_mode='HTML')

def estadisticasxpais(bot, update):
    print("Paso por aca estadísticas.......................................................................")
    user = update.message.from_user

    if update.message.chat_id in consulta_pais:
        consulta_pais[update.message.chat_id]['consulta'] = update.message.text
        pais = update.message.text
    else:
        cancel_estadistica_pais(bot, update)
    logger.info("Estadística de país consultado por %s: %s" % (user.username, update.message.text))

    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    reply_markup = keyboard_correspondiente(update.message.chat_id)


    query = "SELECT * FROM T_TELEGRAM_ESTADISTICAS WHERE pais= '" + str(pais) + "' ;" 
    data=fetch_data(query)
    if len(data)>0:
        texto = "País:  <b>" + str(data[0]['pais']) + " </b>\n"
        if str(data[0]['total_casos']) != '':
            texto += "Total_Casos:  <b> " + str(data[0]['total_casos']) + " </b>\n"
        if str(data[0]['nuevos_casos']) != '':
            texto += "Nuevos_Casos:  <b> " + str(data[0]['nuevos_casos']) + " </b>\n"
        if str(data[0]['total_muertes']) != '':
            texto += "Total_Muertes:  <b> " + str(data[0]['total_muertes']) + " </b>\n"
        if str(data[0]['nuevas_muertes']) != '':
            texto += "Nuevas_Muertes:  <b> " + str(data[0]['nuevas_muertes']) + " </b>\n"
        if str(data[0]['total_sanados']) != '':
            texto += "Total_Sanados:  <b> " + str(data[0]['total_sanados']) + " </b>\n"
        if str(data[0]['casos_activos']) != '':
            texto += "Casos_Activos:  <b> " + str(data[0]['casos_activos']) + " </b>\n"
        if str(data[0]['serios_criticos']) != '':
            texto += "Serios_Criticos:  <b> " + str(data[0]['serios_criticos']) + " </b>\n"
        if str(data[0]['totcasos_1mpop']) != '':
            texto += "Tot. Casos/1Mpop:  <b> " + str(data[0]['totcasos_1mpop']) + " </b>\n"
        if str(data[0]['totmuertes_1mpop']) != '':
            texto += "Tot. Muertes/1Mpop:  <b> " + str(data[0]['totmuertes_1mpop']) + " </b>\n"
        if str(data[0]['total_tests']) != '':
            texto += "Total Tests:  <b> " + str(data[0]['total_tests']) + " </b>\n"
        if str(data[0]['tests_1mpop']) != '':
            texto += "Tot. Tests/1Mpop:  <b> " + str(data[0]['tests_1mpop']) + " </b>\n"

    else:
        texto = "No pudimos encontrar el país. Por Favor, intente nuevamente."

    if update.message.chat_id in consulta_pais:
        del consulta_pais[update.message.chat_id]

    bot.sendMessage(chat_id=update.message.chat_id ,text=texto , reply_markup=reply_markup, parse_mode='HTML')

    return ConversationHandler.END


def estadisticas_provincial(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    bot.sendDocument(chat_id=update.message.chat_id, document=open('selenium/argentina_statics.pdf', 'rb'), reply_markup=reply_markup);

def sismos_mundial(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    bot.sendDocument(chat_id=update.message.chat_id, document=open('selenium/sismos_mundial.pdf', 'rb'), reply_markup=reply_markup);

def manual_bot(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    #if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
    #     reply_markup = keyboard_correspondiente(update.message.chat_id)
    #     update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
    #     return
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    bot.sendDocument(chat_id=update.message.chat_id, document=open('selenium/manual_bot.pdf', 'rb'), reply_markup=reply_markup);


def estadisticas(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    bot.sendDocument(chat_id=update.message.chat_id, document=open('selenium/world_statics.pdf', 'rb'), reply_markup=reply_markup);

def agregar_usuario(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if permission('admin', update.message.chat_id):
        rr = update.message.text.split(' ')[1]
        try:
            miid = int(rr)
            if alta_usuario(miid):
                update.message.reply_text(AGREGAR_USUARIO_OK)
            else:
                update.message.reply_text(AGREGAR_USUARIO_ERROR)
        except Exception as e:
            update.message.reply_text(AGREGAR_USUARIO_ERROR)
            return

def start(bot, update):
    #print(update.message.text)
    # Whois
    telegram_log(update.message.text, update.message.chat_id, update.message.chat['username'], update.message.chat['first_name'])

    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return

    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return

    if not valid_perfil(update.message.chat_id):
        # Respuesta para cuando el user tiene alias pero no tiene completo el perfil
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(ACTUALIZAR_PERFIL,reply_markup=reply_markup)
        return
 
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    update.message.reply_text(SALUDO_EXPLICACION_CON_PERFIL,reply_markup=reply_markup)


def button(bot, update):
    global config
    canal = config.get('Canal','Canal')
    #logger.debug("Button callback")
    query = update.callback_query
    readable = query['message']['text']
    idindex = readable.find('ID:')
    if idindex<0:
        return ''
    enterindex = readable[idindex+4:].find('\n')
    if enterindex==-1:
        idProduct = readable[idindex+4:]
    else:
        idProduct = readable[idindex+4:idindex+enterindex+4]
    idMsg = query['message']['message_id']
    
    consulta = "select * from T_TELEGRAM_PRODUCTOS where id="+str(idProduct)
    producto = fetch_data(consulta)
    product = producto[0]
    fphoto = str(config.get('Pics','Url'))
    fphoto += str(product['dsImg'])
    texto = "Descripción: " + str(product['dsDescription'])
    if str(product['dsImg']) != 'sin_foto.jpg':
        texto += "\n" + fphoto
    
    qmensaje = "select mobile, idMsg from T_TELEGRAM_MENSAJES where idProduct="+str(idProduct)
    mensaje = fetch_data(qmensaje)

    if query['data'] == '/aceptar':
        if aceptar(idProduct):
            qavisar = "select mobile from T_TELEGRAM_PRODUCTOS where id="+str(product['id'])
            mobile = fetch_data(qavisar)[0]['mobile']
            reply_markup = keyboard_correspondiente(mobile)
            confirm = "La publicación de <b>" + str(product['dsDescription']) + "</b> ha sido Aprobada y en breves será publicada en el canal " + str(canal)
            msg = bot.sendMessage(chat_id=mobile ,text=confirm , reply_markup=reply_markup, parse_mode='HTML')
            qaction = "update T_TELEGRAM_MENSAJES set action='ACEPTADO' where idMsg='"+str(idMsg)+"'"
            fetch_data(qaction)
            for m in mensaje:
                user = m['mobile']
                idMsg = m['idMsg']            
                bot.editMessageText(chat_id=user , message_id=idMsg, text=texto + "\n\n  ACEPTADO")
    elif query['data'] == '/rechazar':
        if rechazar(idProduct):
            qavisar = "select mobile from T_TELEGRAM_PRODUCTOS where id="+str(product['id'])
            mobile = fetch_data(qavisar)[0]['mobile']
            reply_markup = keyboard_correspondiente(mobile)
            confirm = "La publicación de <b>" + str(product['dsDescription']) + "</b> ha sido Rechazada."
            msg = bot.sendMessage(chat_id=mobile ,text=confirm , reply_markup=reply_markup , parse_mode='HTML')
            qaction = "update T_TELEGRAM_MENSAJES set action='RECHAZADO' where idMsg='"+str(idMsg)+"'"
            fetch_data(qaction)
            for m in mensaje:
                user = m['mobile']
                idMsg = m['idMsg']            
                bot.editMessageText(chat_id=user , message_id=idMsg, text=texto + "\n\n  RECHAZADO")
     

def help(bot, update):
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    update.message.reply_text("Ingrese /start para comenzar.")
    if permission('admin', update.message.chat_id):
        update.message.reply_text("Recuerde que para ingresar un nuevo usuario debe escribir: /agregar_usuario <miid>")

def error(bot, update, error):
    logging.warning('Update "%s" caused error "%s"' % (update, error))

def message(bot, update):
    logger.warning('Unexpected message')
    telegram_log(update.message.text, update.message.chat_id,
                 update.message.chat['username'],update.message.chat['first_name'])
    if permission('banned',update.message.chat_id):
        # Respuesta para start cuando el usuario esta banned
        update.message.reply_text(BANNED_MSG,reply_markup=ReplyKeyboardRemove())
        return
    if not permission('alias', update.message.chat_id):
        # Respuesta para start cuando no se conoce el usuario
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(SALUDO_SIN_ALIAS,reply_markup=reply_markup)
        return
    if not valid_perfil(update.message.chat_id):
        # Respuesta para cuando el user tiene alias pero no tiene completo el perfil
        reply_markup = keyboard_correspondiente(update.message.chat_id)
        update.message.reply_text(ACTUALIZAR_PERFIL,reply_markup=reply_markup)
        return
    reply_markup = keyboard_correspondiente(update.message.chat_id)
    update.message.reply_text(UTILICE_TECLADO, reply_markup=reply_markup)
        

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Read confir form file
config = ConfigParser.SafeConfigParser()
config.read('main.ini')

# Create the Updater and pass it your bot's token.
token = str(config.get('Telegram','token'))
updater = Updater(token, workers=10)
updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(RegexHandler('miid', miid))
updater.dispatcher.add_handler(RegexHandler('Estadisticas Total', estadisticas))
updater.dispatcher.add_handler(RegexHandler('Estadisticas Argentina', estadisticas_argentina))
updater.dispatcher.add_handler(RegexHandler('Estadisticas Provincia', estadisticas_provincial))
updater.dispatcher.add_handler(RegexHandler('Estadisticas Mundial', estadisticas_mundial))
updater.dispatcher.add_handler(RegexHandler('Sismos Mundial', sismos_mundial))
updater.dispatcher.add_handler(RegexHandler('Manual Bot', manual_bot))
updater.dispatcher.add_handler(RegexHandler('cuantos', cuantos))
#updater.dispatcher.add_handler(CommandHandler('agregar_usuario', agregar_usuario))
updater.dispatcher.add_handler(CallbackQueryHandler(button))
updater.dispatcher.add_handler(CommandHandler('help', help))
updater.dispatcher.add_error_handler(error)

# Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
conv_handler = ConversationHandler(
    entry_points=[RegexHandler('Noticia', product)],

    states={
        DESCRIPCION: [MessageHandler(Filters.text, descripcion_producto)],
        PHOTO: [MessageHandler(Filters.photo, foto_producto),
                CommandHandler('sin_foto', skip_foto_producto)]
    },

    fallbacks=[CommandHandler('cancelar_noticia', cancel)],
    allow_reentry=True,
    run_async_timeout=1,
    timed_out_behavior=[CommandHandler('timeout', timeout)]
)

# Add conversation handler with the states PROFILE, NAME, COUNTRY, PROVINCE
conv_handler_perfil = ConversationHandler(
    entry_points=[RegexHandler('Perfil', profile)],

    states={
        NOMBRE: [MessageHandler(Filters.text, nombre_perfil )],
        PAIS: [MessageHandler(Filters.text, pais_perfil )],
        PROVINCIA: [MessageHandler(Filters.text, provincia_perfil )]
    },

    fallbacks=[CommandHandler('cancelar_perfil', cancel_profile)],
    allow_reentry=True,
    run_async_timeout=1,
    timed_out_behavior=[CommandHandler('timeout', timeout)]
)

# Add conversation handler CONSULTAS
conv_handler_consulta = ConversationHandler(
    entry_points=[RegexHandler('Consultas', question)],

    states={
         CONSULTA : [MessageHandler(Filters.text, input_question )]
    },

    fallbacks=[CommandHandler('cancelar_consulta', cancel_question)],
    allow_reentry=True,
    run_async_timeout=1,
    timed_out_behavior=[CommandHandler('timeout', timeout)]
)


# Add conversation handler CONSULTASxPAIS
conv_handler_consulta_pais = ConversationHandler(
    entry_points=[RegexHandler('EstadisticasxPais', question_estadistica_pais)],

    states={
         CONSULTA_PAIS : [MessageHandler(Filters.text, estadisticasxpais )]
    },

    fallbacks=[CommandHandler('cancelar_estadistica_pais', cancel_estadistica_pais)],
    allow_reentry=True,
    run_async_timeout=1,
    timed_out_behavior=[CommandHandler('timeout', timeout)]
)

updater.dispatcher.add_handler(conv_handler)
updater.dispatcher.add_handler(conv_handler_perfil)
updater.dispatcher.add_handler(conv_handler_consulta)
updater.dispatcher.add_handler(conv_handler_consulta_pais)
updater.dispatcher.add_handler(MessageHandler(Filters.all,message))

t = threading.Thread(name='response_consultas', target=response_consultas)
t.setDaemon(True)
t.start()

# Create the Updater and pass it your bot's token.
token = str(config.get('Telegram','token'))
bot = telegram.Bot(token=token)

# Start the Bot
updater.start_polling()


# Run the bot until the user presses Ctrl-C or the process receives SIGINT,
# SIGTERM or SIGABRT
updater.idle()
