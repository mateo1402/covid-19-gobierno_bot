import  re, os, ConfigParser, urllib2, atom.service,  atom, getopt, string, time, sys, string, datetime, random, json, gspread
from oauth2client.client import SignedJwtAssertionCredentials
import unicodedata
import MySQLdb, datetime, time
import ssl
import threading

# encoding=utf8
reload(sys)
sys.setdefaultencoding('utf8')

try:
   from xml.etree import ElementTree
except ImportError:
   from elementtree import ElementTree

def fetch_data(query):
    global config
    try:
        db = MySQLdb.connect(host=config.get('Db','Host'), port=int(config.get('Db','Port')) , user=config.get('Db','Usuario'), passwd=config.get('Db','Contrasena'), db=config.get('Db','Db'), charset='utf8')
        db.autocommit(1)
        cursor=db.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)
        data=cursor.fetchall()
        cursor.close()
        db.close()
        return data
    except Exception, e:
        #reconnect()
        print query
        print 'MySQL Exception, trying again...', e
        #fetch_data(query)	

def setcell(r,v):
        r.value = v


# OPEN: estado inicial de la preguntai, se postea al GSS
# WAITINGFORRESPONSE se pusheo a GSS
# ANSWERED: cuando esta respondida en el GSS, se updatea respuesta y estado en DB
# UPDATEINGUSER: cuando el GSS ya actualizo la DB y esta a la espera de enviar respuesta al usuario
# CLOSED: cuando telegram notifico al usuario la respuesta


def leer_de_google(wks):
        while True:
            list_of_lists = wks.get_all_records(empty2zero=False, head=1)
            fila=1
            for row in list_of_lists:
                fila=fila+1
                if row['Estado_consulta']=='ANSWERED' :
                    try:
                        query="update T_TELEGRAM_CONSULTAS set texto_respuesta='"+str(row['Texto_respuesta'])+"', estado_consulta='UPDATEINGUSER', fecha_respuesta=now() where id="+str(row['Consulta_ID'])
                        fetch_data(query)
                        wks.update_cell(fila, 6,'UPDATEINGUSER')
                    except Exception ,e:
                        print e
            time.sleep(2)


# Main
###########
config = ConfigParser.SafeConfigParser()
archivo_config=sys.argv[0].replace('.py','.ini')
config.read(archivo_config)


gss_name=config.get('Google_Docs','gss_name')
gss_sheet_request=config.get('Google_Docs','gss_sheet_request')


#Connect to Google
json_key = json.load(open('bbsoft-rpa-a35e11c0f7ee.json'))
scope = ['https://spreadsheets.google.com/feeds']
credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'], scope)
gc = gspread.authorize(credentials)
wks = gc.open(gss_name).sheet1

#t = threading.Thread(name='leer_de_google', target=leer_de_google(wks))
#t.setDaemon(True)
#t.start()

#leer_de_google(wks)
#sys.exit(1)

count=4
while (count):
    try:
        leer_de_google(wks)
        time.sleep(random.randint(5, 15))
    except:
        count=count-1

