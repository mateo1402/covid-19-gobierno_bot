import  re, os, ConfigParser, urllib2, atom.service,  atom, getopt, string, time, sys, string, datetime, random, json, gspread
from oauth2client.client import SignedJwtAssertionCredentials
import unicodedata
import MySQLdb, datetime, time
import ssl
import threading

# encoding=utf8
reload(sys)
sys.setdefaultencoding('utf8')

try:
   from xml.etree import ElementTree
except ImportError:
   from elementtree import ElementTree

def fetch_data(query):
    global config
    try:
        db = MySQLdb.connect(host=config.get('Db','Host'), port=int(config.get('Db','Port')) , user=config.get('Db','Usuario'), passwd=config.get('Db','Contrasena'), db=config.get('Db','Db'), charset='utf8')
        db.autocommit(1)
        cursor=db.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute(query)
        data=cursor.fetchall()
        cursor.close()
        db.close()
        return data
    except Exception, e:
        #reconnect()
        print query
        print 'MySQL Exception, trying again...', e
        #fetch_data(query)	

def setcell(r,v):
        r.value = v

def leer_de_la_db(wks):
    while True:
        query="select c.id, c.fecha_consulta, CAST(CONVERT(c.texto_consulta USING utf8) AS BINARY) as texto_consulta , CAST(CONVERT(n.first_name USING utf8) AS BINARY) as first_name, 'Basico' as plan from T_TELEGRAM_CONSULTAS c join T_TELEGRAM_LOG_NOMBRE n on (c.telegram_mobile=n.mobile)  where estado_consulta in ('OPEN');"
        consultas=fetch_data(query)
        if len(consultas)>0:
            list_of_lists = wks.get_all_records(empty2zero=False, head=1)
            fila=1
            for row in list_of_lists:
                if row['Consulta_ID']<>'': fila=fila+1
                else: break
            for consulta in consultas:
                try:
                    fila=fila+1
                    rango='A'+str(fila)+':E'+str(fila)
                    cell_list=wks.range(rango)
                    content=[consulta['id'],str(consulta['first_name']),consulta['plan'],consulta['fecha_consulta'],consulta['texto_consulta']]
                    map(setcell,cell_list,content)
                    wks.update_cells(cell_list)
                    query="update T_TELEGRAM_CONSULTAS set estado_consulta='WAITINGFORRESPONSE' where id="+str(consulta['id'])
                    fetch_data(query)
                except Exception,e:
                    print e
        time.sleep(2)

# OPEN: estado inicial de la preguntai, se postea al GSS
# WAITINGFORRESPONSE se pusheo a GSS
# ANSWERED: cuando esta respondida en el GSS, se updatea respuesta y estado en DB
# UPDATEINGUSER: cuando el GSS ya actualizo la DB y esta a la espera de enviar respuesta al usuario
# CLOSED: cuando telegram notifico al usuario la respuesta


# Main
###########
config = ConfigParser.SafeConfigParser()
archivo_config=sys.argv[0].replace('.py','.ini')
config.read(archivo_config)


gss_name=config.get('Google_Docs','gss_name')
gss_sheet_request=config.get('Google_Docs','gss_sheet_request')


#Connect to Google
json_key = json.load(open('bbsoft-rpa-a35e11c0f7ee.json'))
scope = ['https://spreadsheets.google.com/feeds']
credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'], scope)
gc = gspread.authorize(credentials)
wks = gc.open(gss_name).sheet1

#t = threading.Thread(name='leer_de_la_db', target=leer_de_la_db(wks))
#t.setDaemon(True)
#t.start()

count=4
while (count):
    try:
        leer_de_la_db(wks)
        time.sleep(random.randint(5, 15))
    except:
        count=count-1

